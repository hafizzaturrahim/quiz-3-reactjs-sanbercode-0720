import React, { useContext, createContext } from "react";
import { Switch, Link, Route } from "react-router-dom";
import './public/css/style.css'

import Nav from "./nav";
import About from "./about";
import Movie from "./Movie/movie";
import MovieHome from "./Movie/movieHome";
import { MovieProvider } from "./Movie/movieContext";

import Login from "./Auth/login";
import Logout from "./Auth/logout";
import { AuthProvider } from "./Auth/authContext";


const Routes = () => {
  
  return (
    <>
      <AuthProvider>
        <Nav />
        <section>
          <Switch>
            <Route exact path="/about">
              {/* <Timer start={100}/> */}
              <About />
            </Route>
            <Route exact path="/movie">
              <Movie />
            </Route>
            <Route exact path="/login">
              <Login />
            </Route>
            <Route exact path="/logout">
              <Logout />
            </Route>
            <Route exact path="/">
              <MovieProvider>
                <MovieHome />
              </MovieProvider>
            </Route>
          </Switch>
        </section>

        <footer>
          <h5>copyright &copy; 2020 by Sanbercode</h5>
        </footer>
      </AuthProvider>
    </>
  );
};

export default Routes;
