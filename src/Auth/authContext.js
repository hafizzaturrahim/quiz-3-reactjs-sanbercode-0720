import React, { useState, createContext } from "react";

export const AuthContext = createContext();
export const AuthProvider = props => {
    const [authData, setauthData] = useState({
        isLogin : false,
        id:""
    })

    return (
        <AuthContext.Provider value={[authData, setauthData]}>
            {props.children}
        </AuthContext.Provider>
    );
}