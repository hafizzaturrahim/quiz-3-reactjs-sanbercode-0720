import React, { useState, useContext } from "react";
import { AuthContext } from "./authContext";
import { Link, useHistory } from "react-router-dom";

const Logout = () => {
    const [authData, setauthData] = useContext(AuthContext)
    setauthData({isLogin:false, id:""})
    const history = useHistory();
    history.push("/login");

    return(null)

}
export default Logout