import React, { useState, useContext } from "react";
import { AuthContext } from "./authContext";
import { Link, useHistory } from "react-router-dom";


const Login = () => {
    const [authData, setauthData] = useContext(AuthContext)
    console.log("islogin awal")
    console.log(authData.isLogin)
    const listUser = [
        {id : 1, username : "admin", password : "admin"},
        {id : 2, username : "hafizh", password : "1234"}]

    const [notif, setnotif] = useState("")
    const history = useHistory();


    const handleLogin = (event) =>{
        event.preventDefault()

        let username = event.target.username.value
        let password = event.target.password.value
        
        let user = listUser.find(el => el.username === username)
        if(user !==undefined && password === user.password){
            setauthData({isLogin:true, id:user.id})
            console.log("masuk")
            history.push("/");
        }else{
            setnotif("username atau password salah")
        }
    }

    return (
        <form onSubmit={handleLogin} method="post">
            <div>
                <label>Username :</label>
                <input type="text" name="username" required/>
                <br />
                <br />
                <label>Password :</label>
                <input type="password" name="password" required/>
                <br />
                <br />
                <button className="btn-width">Login</button>
                { notif !== null && 
                <p style={{color:"red"}}>{notif} </p>
                }
            </div>
        </form>
    )
}

export default Login
