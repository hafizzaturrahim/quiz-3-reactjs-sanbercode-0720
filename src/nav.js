import React, { useContext } from "react";
import { Link } from "react-router-dom";
import { AuthContext } from "./Auth/authContext";

const Nav = () =>{
    const [authData, setauthData] = useContext(AuthContext)

    return(
        <header>
        <img id="logo" src={require('./public/img/logo.png')} style={{ width: "200px" }} />
        <nav>

          <ul>
            <li>
              <Link to="/">Home</Link>
            </li>

            <li>
              <Link to="/about">About</Link>
            </li>
            { authData.isLogin === true && 
            <>
            <li>
              <Link to="/movie">Movie List Editor</Link>
            </li>
            <li>
            <Link to="/logout">Logout</Link>
            </li>
            </>
            }
            { authData.isLogin === false && 
            <>
            <li>
              <Link to="/login">Login</Link>
            </li>
            </>
            }
          </ul>
        </nav>
      </header>
    )
}

export default Nav