import React, { useContext } from "react"
import MovieList from "./movieList"
import MovieForm from "./movieForm"
import { MovieProvider } from "./movieContext"
import { InputProvider } from "./inputContext"

import { AuthContext } from "../Auth/authContext";
import { Link, useHistory } from "react-router-dom";

const Movie = () =>{
  const [authData, setauthData] = useContext(AuthContext)
  const history = useHistory();
  
  
  if(authData.isLogin == false){
    history.push("/login");
  }

  return(
    <MovieProvider>
      <InputProvider>
      <MovieList/>
      <MovieForm/>
      </InputProvider>
    </MovieProvider>
  )
}

export default Movie