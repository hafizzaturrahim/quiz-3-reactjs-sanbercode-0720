import React, { useContext } from 'react';
import { MovieContext } from './movieContext';

const MovieHome = () => {
    const [listMovie, setlistMovie] = useContext(MovieContext)
    console.log(listMovie)
   
    let listMoviebyRating = null
    if(listMovie !== null)  {
        listMoviebyRating= listMovie.sort(function(a, b){
            return b.rating-a.rating
        })
        console.log("byrating")
        console.log(listMoviebyRating)
    }

    return (
        <div>
            <h1>Daftar Film - Film Terbaik</h1>
            <div id="article-list">
                {listMovie !== null && listMoviebyRating !==null && listMoviebyRating.map((movieObj, index) => {
                        return (
                            <article>
                                <a><h3>{movieObj.title}</h3></a>
                                <h4>Rating {parseFloat(movieObj.rating).toFixed(1)} </h4>
                                <h4>Durasi {movieObj.duration} </h4>
                                <h4>Genre : {movieObj.genre} </h4>
                                <p><b>Deskripsi : </b> {movieObj.description}
                                </p>
                            </article>
                        )
                    })
                }
            </div>
        </div>
    )
}

export default MovieHome