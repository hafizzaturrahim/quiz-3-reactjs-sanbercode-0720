import React, { useState, useEffect, createContext } from "react";
import axios from 'axios';

export const MovieContext = createContext();
export const MovieProvider = props => {
    const [listMovie, setlistMovie] = useState(null)
    
    useEffect(() => {
        if (listMovie === null) {
            axios.get(`http://backendexample.sanbercloud.com/api/movies`)
                    .then(res => {
                        console.log("read data")
                        setlistMovie(res.data.map(el => {
                            return {
                                id: el.id,
                                title: el.title,
                                description: el.description,
                                year: el.year,
                                duration: el.duration,
                                genre: el.genre,
                                rating:el.rating
                            }
                        }))
                        
                    })
        }

    }, [listMovie])
    
    return (
        <MovieContext.Provider value={[listMovie, setlistMovie]}>
            {props.children}
        </MovieContext.Provider>
    );
};