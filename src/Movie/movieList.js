import React, { useContext } from "react"
import { MovieContext } from './movieContext';
import { InputContext } from "./inputContext"
import axios from 'axios';

const MovieList = () => {
    const [listMovie, setlistMovie] = useContext(MovieContext)
    const [inputMovie, setinputMovie, selectedID, setselectedID] = useContext(InputContext)

    const handleEdit = (event) => {
        let idMovie = parseInt(event.target.value)
        let movie = listMovie.find(x => x.id === idMovie)
        setinputMovie({
            title : movie.title,
            description : movie.description,
            year : movie.year,
            duration : movie.duration,
            genre : movie.genre,
            rating : movie.rating
        })
        setselectedID(idMovie)
    }

    const handleDelete = (event) => {
        let idMovie = parseInt(event.target.value)
        let newListMovie = listMovie.filter(el => el.id !== idMovie)

        axios.delete(`http://backendexample.sanbercloud.com/api/movies/${idMovie}`)
            .then(res => {
                console.log(res)
            })

        setlistMovie([...newListMovie])
    }

    return (
        <div>
            <h1>Daftar Film</h1>
            <table>
                <thead>
                    <tr>
                        <th>No.</th>
                        <th>Judul Film</th>
                        <th>Deskripsi</th>
                        <th>Tahun</th>
                        <th>Durasi</th>
                        <th>Genre</th>
                        <th>Rating</th>
                        <th>Aksi</th>
                    </tr>
                </thead>
                <tbody>
                    {
                        listMovie !== null && listMovie.map((movieObj, index) => {
                            return (
                                <tr>
                                    <td>{index + 1}</td>
                                    <td>{movieObj.title}</td>
                                    <td>{movieObj.description}</td>
                                    <td>{movieObj.year}</td>
                                    <td>{movieObj.duration}</td>
                                    <td>{movieObj.genre}</td>
                                    <td>{parseFloat(movieObj.rating).toFixed(1)}</td>
                                    <td>
                                        <button onClick={handleEdit} value={movieObj.id}>Edit</button>
                                        <button onClick={handleDelete} value={movieObj.id}>Delete</button>
                                    </td>
                                </tr>
                            )
                        })
                    }
                </tbody>
            </table>
        </div>

    )
}

export default MovieList

