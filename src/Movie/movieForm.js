import React, { useContext } from "react";
import { MovieContext } from './movieContext';
import { InputContext } from "./inputContext";
import axios from 'axios';

const MovieForm = () => {
    const [listMovie, setlistMovie] = useContext(MovieContext)
    const [inputMovie, setinputMovie, selectedID, setselectedID] = useContext(InputContext)

    const handleChange = (event) => {
        let typeOfInput = event.target.name

        switch (typeOfInput) {
            case "title":
                {
                    setinputMovie({ ...inputMovie, title: event.target.value });
                    break
                }
            case "description":
                {
                    setinputMovie({ ...inputMovie, description: event.target.value });
                    break
                }
            case "year":
                {
                    setinputMovie({ ...inputMovie, year: event.target.value });
                    break
                }
            case "duration":
                {
                    setinputMovie({ ...inputMovie, duration: event.target.value });
                    break
                }
            case "genre":
                {
                    setinputMovie({ ...inputMovie, genre: event.target.value });
                    break
                }
            case "rating":
                {
                    let rating = parseInt(event.target.value)
                    if(rating < 1 || rating > 10){
                        rating = 1
                    }
                    setinputMovie({ ...inputMovie, rating});
                    break
                }
            default:
                { break; }
        }
    }

    const handleSubmit = (event) => {
        // menahan submit
        event.preventDefault()

        let title = inputMovie.title
        let description = inputMovie.description
        let year = inputMovie.year
        let duration = inputMovie.duration
        let genre = inputMovie.genre
        let rating = inputMovie.rating


        if (title.replace(/\s/g, '') !== "" && description.replace(/\s/g, '') !== "" && year.toString().replace(/\s/g, '') !== "" && duration.toString().replace(/\s/g, '') !== "" && genre.replace(/\s/g, '') !== "" && rating.toString().replace(/\s/g, '') !== "") {
            if (selectedID === -1) {
                axios.post(`http://backendexample.sanbercloud.com/api/movies`, { title, description, year, duration, genre, rating })
                    .then(res => {
                        setlistMovie([...listMovie, { id: res.data.id, title, description, year, duration, genre, rating }])
                    })
            } else if (selectedID !== -1) {
                axios.put(`http://backendexample.sanbercloud.com/api/movies/${selectedID}`, { title, description, year, duration, genre, rating })
                    .then(() => {
                        let movie = listMovie.find(el => el.id === selectedID)
                        movie.title = title
                        movie.description = description
                        movie.year = year
                        movie.duration = duration
                        movie.genre = genre
                        movie.rating = rating
                        setlistMovie([...listMovie])
                    })
            }

            setselectedID(-1)
            setinputMovie({ title: "", description: "", year: "", duration: "", genre: "", rating: "" })
        }
    }


    return (
        <div>
            <h1>Form Film</h1>
            <div style={{ width: "80%", margin: "0 auto", display: "block" }}>
                <div style={{ border: "1px solid #aaa", padding: "20px" }}>
                    <form onSubmit={handleSubmit}>
                        <label>
                            Judul Film :
                        </label>
                        <input type="text" name="title" value={inputMovie.title} onChange={handleChange} />
                        <br />
                        <br />
                        <label>
                            Deskripsi:
                        </label>
                        <br />
                        <textarea name="description" value={inputMovie.description} onChange={handleChange} />
                        <br />
                        <br />
                        <label>
                            Tahun :
                        </label>
                        <input type="number" name="year" value={inputMovie.year} onChange={handleChange} />
                        <br />
                        <br />
                        <label>
                            Durasi (dalam menit) :
                        </label>
                        <input type="number" name="duration" value={inputMovie.duration} onChange={handleChange} />
                        <br />
                        <br />
                        <label>
                            Genre :
                        </label>
                        <input type="text" name="genre" value={inputMovie.genre} onChange={handleChange} />
                        <br />
                        <br />
                        <label>
                            Rating (1-10):
                        </label>
                        <input type="number" name="rating" value={inputMovie.rating} onChange={handleChange} placeholder="Isi antara 1- 10"/>
                        <br />
                        <br />
                        <button>submit</button>

                    </form>
                </div>
            </div>
        </div>
    )
}

export default MovieForm